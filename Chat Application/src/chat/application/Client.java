package chat.application;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.net.Socket;
import javax.swing.*;

public class Client extends JFrame implements ActionListener{
    JPanel p1;
    JTextField txt1;
    //JTextArea txt1;
    JButton btn1;
    static JTextArea tarea;
    
    
    static DataOutputStream dos;
    static DataInputStream dis;
            
    Client(){
        
        p1 = new JPanel();
        p1.setLayout(null);
        p1.setBackground(new Color(7, 94, 84));
        p1.setBounds(0, 0, 420, 70);
        add(p1);
        
        ImageIcon img1 = new ImageIcon(ClassLoader.getSystemResource("chat/application/icons/3.png"));
        Image img2 = img1.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT);
        ImageIcon img3 = new ImageIcon(img2);
        JLabel l1 = new JLabel(img3);
        l1.setBounds(5,18,30, 30);
        p1.add(l1);
        
        l1.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent me){
                System.exit(0);
            }
        });
        
        ImageIcon img4 = new ImageIcon(ClassLoader.getSystemResource("chat/application/icons/bristy.PNG"));
        Image img5 = img4.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT);
        ImageIcon img6 = new ImageIcon(img5);
        JLabel l2 = new JLabel(img6);
        l2.setBounds(40,8,60, 60);
        p1.add(l2);
        
        ImageIcon img7 = new ImageIcon(ClassLoader.getSystemResource("chat/application/icons/video.PNG"));
        Image img8 = img7.getImage().getScaledInstance(25, 25, Image.SCALE_DEFAULT);
        ImageIcon img9 = new ImageIcon(img8);
        JLabel l3 = new JLabel(img9);
        l3.setBounds(270,20,25, 25);
        p1.add(l3);
        
        ImageIcon img11 = new ImageIcon(ClassLoader.getSystemResource("chat/application/icons/phone.PNG"));
        Image img12 = img11.getImage().getScaledInstance(35, 35, Image.SCALE_DEFAULT);
        ImageIcon img13 = new ImageIcon(img12);
        JLabel l4 = new JLabel(img13);
        l4.setBounds(320,20,23, 23);
        p1.add(l4);
        
        ImageIcon img14 = new ImageIcon(ClassLoader.getSystemResource("chat/application/icons/3icon.png"));
        Image img15 = img14.getImage().getScaledInstance(13, 25, Image.SCALE_DEFAULT);
        ImageIcon img16 = new ImageIcon(img15);
        JLabel l5 = new JLabel(img16);
        l5.setBounds(370,20,13, 25);
        p1.add(l5);
        
        JLabel l6 = new JLabel("Bristy");
        l6.setFont(new Font("SAN_SERIF", Font.BOLD, 16));
        l6.setForeground(Color.WHITE);
        l6.setBounds(110, 15, 100, 18);
        p1.add(l6);
        
        JLabel l7 = new JLabel("Online");
        l7.setFont(new Font("SAN_SERIF", Font.PLAIN, 10));
        l7.setForeground(Color.WHITE);
        l7.setBounds(110, 35, 100, 18);
        p1.add(l7);
        
        tarea = new JTextArea();
        tarea.setBounds(5, 75, 410, 575);
        
        tarea.setForeground(Color.BLACK);
        tarea.setFont(new Font("SAN_SERIF", Font.PLAIN, 22));
        tarea.setEditable(false);
        tarea.setLineWrap(true);
        tarea.setWrapStyleWord(true);
        add(tarea);
        
        
        txt1 = new JTextField();
        //txt1 = new JTextArea();
        txt1.setBounds(5, 660, 300,35);
        txt1.setFont(new Font("SAN_SERIF", Font.PLAIN, 14));
        
        add(txt1);
        
        btn1 = new JButton("Send");
        btn1.setBounds(310, 662, 105, 30);
        btn1.setBackground(new Color(7, 94, 84));
        btn1.setForeground(Color.WHITE);
        btn1.setFont(new Font("SAN_SERIF", Font.PLAIN, 14));
        btn1.addActionListener(this);
        add(btn1);
        
        setLayout(null);
        setSize(420, 700);
        setLocation(1000, 200);
        setUndecorated(true);
        
        setVisible(true);
        
    }
    
    public void actionPerformed(ActionEvent ae){
        try{
        String out = tarea.getText();
        //out+= "Bristy:\n"+txt1.getText()+"\n";
        tarea.setText(tarea.getText()+"\n"+out);
        dos.writeUTF(out);
        txt1.setText("");
        }catch(Exception e){
            
        }
                
    }
    
    public static void main(String[] args){
        new Client().setVisible(true);
        
        try{
            Socket s = new Socket("localhost", 2929);
            
            dis = new DataInputStream(s.getInputStream());
            dos = new DataOutputStream(s.getOutputStream());
            
            String inputMsg = "";
            
            inputMsg = dis.readUTF();
            tarea.setText(tarea.getText()+"\n"+inputMsg);
            
        }catch(Exception e){
            
        }
    }
    



}
